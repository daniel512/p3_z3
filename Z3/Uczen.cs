﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Z3
{
    public class Uczen
    {
        private string _imie, _nazwisko;
        private List<Ocena> _oceny;
        public static string[] _przedmioty = { "matematyka", "programowanie", "bazy danych" };
        public string Imie
        {
            get { return _imie; }
            set { _imie = value; }
        }

        public string Nazwisko
        {
            get  { return _nazwisko; }
            set  { _nazwisko = value; }
        }

        public Uczen(string imie, string nazwisko)
        {
            this.Imie = imie;
            this.Nazwisko = nazwisko;
            this._oceny = new List<Ocena>();
        }

        public void NowaOcena(Ocena ocena)
        {
            foreach(var przedmiot in _przedmioty)
            {
                if(ocena.Przedmiot.ToLower() == przedmiot)
                {
                    _oceny.Add(ocena);
                    return;
                }
            }
            Console.WriteLine("Nie ma takiego przedmiotu");
        }

        public void WypiszOceny()
        {
            Console.WriteLine($"Oceny {this.Imie} {this.Nazwisko}");
            Console.WriteLine("___________________________________");
            foreach(var ocena in _oceny) 
            {
                Console.WriteLine($"{ocena.Przedmiot}: {ocena.Wartosc}, waga {ocena.Waga}");
            }
            Console.WriteLine("___________________________________");
        }

        public float sredniaPrzedmiotu(string przedmiot)
        {
            float suma = 0;
            float waga = 0;

            foreach (var ocena in _oceny)
            {      
                if(przedmiot.ToLower() == ocena.Przedmiot)
                {
                    suma += ocena.Wartosc * ocena.Waga;
                    waga += ocena.Waga;
                }
            }
            return suma / waga;
        }

        public float sredniaSemestralna()
        {
            float suma = 0;
            float waga = 0;

            foreach (var ocena in _oceny)
            {
                suma += ocena.Wartosc * ocena.Waga;
                waga += ocena.Waga;
            }

            return suma / waga;
        }

    }
}

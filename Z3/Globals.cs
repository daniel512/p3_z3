﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Z3
{
    public static class Globals
    {

        /*Zwraca ucznia z najwyzsza srednia semestralna z uczniów podanych jako parametry*/
        public static Uczen NajlepszyUczen(params Uczen[] uczniowie)
        {
            float najlepszaSrednia = 0;
            Uczen najlepszy = new Uczen("","");

            for(int i = 0; i < uczniowie.Length; i++)
            {
                if(i == 0)
                {
                    najlepszaSrednia = uczniowie[i].sredniaSemestralna();
                    najlepszy = uczniowie[i];
                }

                if(uczniowie[i].sredniaSemestralna() > najlepszaSrednia)
                {
                    najlepszaSrednia = uczniowie[i].sredniaSemestralna();
                    najlepszy = uczniowie[i];
                }
            }

            return najlepszy;
        }
    }
}

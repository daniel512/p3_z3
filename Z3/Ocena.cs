﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Z3
{
    public class Ocena
    {
        private float _wartosc, _waga;
        private string _przedmiot;
        public static int WszystkieOceny;

        public float Wartosc
        {
            get { return _wartosc; }
            set { _wartosc = value; }
        }

        public float Waga
        {
            get { return _waga; }
            set { _waga = value; }
        }

        public string Przedmiot
        {
            get  { return _przedmiot; }
            set { _przedmiot = value; }
        }

       

        public Ocena(int wartosc, int waga, string przedmiot)
        {
            this.Wartosc = wartosc;
            this.Waga = waga;
            this.Przedmiot = przedmiot;
            WszystkieOceny++;
        }


    }
}

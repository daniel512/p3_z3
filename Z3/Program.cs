﻿using System;

namespace Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            Uczen maciek = new Uczen("Maciej", "Kowalski");
            Uczen andrzej = new Uczen("Andrzej","Nowak");
            Uczen krzysztof = new Uczen("Krzysztof","Kowalski");

            maciek.NowaOcena(new Ocena(5,3,"matematyka"));
            maciek.NowaOcena(new Ocena(6, 1, "matematyka"));
            maciek.NowaOcena(new Ocena(1, 2, "matematyka"));
            maciek.NowaOcena(new Ocena(4, 4, "programowanie"));
            maciek.NowaOcena(new Ocena(5, 1, "programowanie"));

            andrzej.NowaOcena(new Ocena(1, 2, "matematyka"));
            andrzej.NowaOcena(new Ocena(4, 2, "bazy danych"));
            andrzej.NowaOcena(new Ocena(5, 1, "programowanie"));
            andrzej.NowaOcena(new Ocena(3, 2, "programowanie"));
            andrzej.NowaOcena(new Ocena(5, 3, "matematyka"));

            krzysztof.NowaOcena(new Ocena(6,2,"bazy danych"));



            Console.WriteLine(maciek.sredniaPrzedmiotu("matematyka"));
            Console.WriteLine(maciek.sredniaSemestralna());
            Console.WriteLine(andrzej.sredniaPrzedmiotu("programowanie"));
            Console.WriteLine(andrzej.sredniaSemestralna());


            
            Console.WriteLine(Globals.NajlepszyUczen(maciek,andrzej, krzysztof).Imie);
        }
    }
}
